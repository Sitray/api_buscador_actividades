const express = require('express');
const router = express.Router();
const withAuth = require('../middleware');

router.get('/', function (req, res, next) {
    res.end("Login back...");
})


router.get('/secret', withAuth, function (req, res, next) {
    res.send("The password is potato...");
})


module.exports = router;