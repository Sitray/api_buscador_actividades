const express = require("express");
const multer = require("multer");
//bcrypt es un modulo que nos permite encriptar en una dirección
const bcrypt = require("bcrypt");

const model = require("../models/index");
const ModelUsuario = model.Usuarios;
const ModelUsuariosView = model.UsuariosView;
const Token = model.Token;

const JWT = require("jsonwebtoken");
const SECRET = "merdolino";
const withAuth = require("../middleware");

//const tokenMaxMin = 30;

const router = express.Router();

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "uploads");
  },
  filename: function(req, file, cb) {
    cb(null, Date.now() + "-" + file.originalname);
  }
});

const upload = multer({ storage: storage }).single("file");

/*important!
si tenim 2 servidors:
access-control-allow-headers ha d'incloure authorization
acces-control-allow-origin ha d'apuntar a la ip/servidor del front
el fetch del client (el login) ha d'incloure "credentials: true" a les propietats 
si és un sol servidor no cal!
*/

/* POST registro de usuario */
router.post("/registre", function(req, res, next) {
  // en la bdd siempre guardamos password encriptado
  // el número 10 es el número de "rounds" en el cálculo, cuanto mayor más tarda en computar pero más difícil es descubrir el password
  // https://stackoverflow.com/questions/46693430/what-are-salt-rounds-and-how-are-salts-stored-in-bcrypt
  const hash = bcrypt.hashSync(req.body.Password, 10);
  //reemplazamos el password con su versión encriptada
  req.body.Password = hash;
  // "create" es un método de sequelize, recibe un objeto con las propiedades/valores de los campos
  ModelUsuario.create(req.body)
    .then(item => res.json({ ok: true, data: item }))
    .catch(error => res.json({ ok: false, error }));
});

/* POST LOGIN */
router.post("/login", (req, res) => {
  //leemos username y password del body
  const { Usuario, Password } = req.body;
  // const username = req.body.nom;
  // const password = req.body.password;
  // si username / password no se han facilitado devolvemos error con código de estado 400
  if (!Usuario || !Password) {
    return res
      .status(400)
      .json({ ok: false, error: "username o password no recibidos" });
  }

  //buscamos usuario y comprobamos si password coincide
  //findOne es un método de sequelize, si no encuentra nada devolverá error
  ModelUsuario.findOne({ where: { Usuario } })
    .then(usuari => {
      //comparamos el password recibido con el password del usuario guardado en bdd, ambos encriptados
      console.log(bcrypt.hashSync(Password, 10));
      console.log(usuari.Password);
      if (bcrypt.compareSync(Password, usuari.Password)) {
        //si ok, devolvemos usuario a siguiente "then"
        return usuari;
      } else {
        // si no coinciden pasamos msg error a "catch"
        throw "password no coincide";
      }
    })
    .then(usuari => {
      //ok, login correcto, creamos un token aleatorio
      let token = "";
      const caractersPossibles =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
      const longitud = 15;
      for (var i = 0; i < longitud; i++) {
        token += caractersPossibles.charAt(
          Math.floor(Math.random() * caractersPossibles.length)
        );
      }
      //devolvemos un nuevo objeto "token" al siguiente then, que incluye id y nombre de usuario
      return Token.create({
        token,
        idUsuari: usuari.id,
        nomusuari: usuari.Usuario,
        img: usuari.Img,
        admin: usuari.Admin
      });
    })
    .then(token => res.json({ ok: true, data: token })) //enviamos respuesta con el token completo en json
    .catch(error => res.json({ ok: false, error: error }));
});
router.post("/ComprobateLogged", (req, res) => {
  const { token } = req.body;
  //si no existe el token no aceptamos logout
  if (!token) {
    return res.status(400).json({ ok: false, error: "token no recibido" });
  }
  Token.findOne({ where: { token } })
    .then(data => res.json({ ok: true, data: data }))
    .catch(error => res.json({ ok: false, error: error }));
});

router.post("/ComprobateAdmin", (req, res) => {
  const { token } = req.body;
  //si no existe el token no aceptamos logout
  if (!token) {
    return res.status(400).json({ ok: false, error: "token no recibido" });
  }
  Token.findOne({ where: { token } })
    .then(User => User.admin)
    .then(data => res.json({ ok: true, data: data }))
    .catch(error => res.json({ ok: false, error: error }));
});

router.post("/VerPerfil", (req, res) => {
  const { token } = req.body;
  //si no existe el token no aceptamos logout
  if (!token) {
    return res.status(400).json({ ok: false, error: "token no recibido" });
  }
  Token.findOne({ where: { token } })
    .then(data => data.idUsuari)
    .then(idUser => ModelUsuariosView.findOne({ where: { id: idUser } }))
    .then(data => res.json({ ok: true, data: data }))
    .catch(error => res.json({ ok: false, error: error }));
});

router.post("/Like", (req, res) => {
  const { token, Eventos_id } = req.body;
  //si no existe el token no aceptamos logout
  if (!token) {
    return res.status(400).json({ ok: false, error: "token no recibido" });
  } else if (!Eventos_id) {
    return res.status(400).json({ ok: false, error: "Evento no recibido" });
  }
  Token.findOne({ where: { token } })
    .then(idToken => idToken.idUsuari)
    .then(idUser =>
      model.Usuarios_Eventos.create({
        Usuarios_id: idUser,
        Eventos_id
      })
    )
    .then(data => res.json({ ok: true, data: data }))
    .catch(error => res.json({ ok: false, error: error }));
});

router.post("/Dislike", (req, res) => {
  const { token, Eventos_id } = req.body;
  //si no existe el token no aceptamos logout
  if (!token) {
    return res.status(400).json({ ok: false, error: "token no recibido" });
  } else if (!Eventos_id) {
    return res.status(400).json({ ok: false, error: "token no recibido" });
  }
  Token.findOne({ where: { token } })
    .then(idToken => idToken.idUsuari)
    .then(Usuarios_id =>
      model.Usuarios_Eventos.destroy({ where: { Usuarios_id, Eventos_id } })
    )
    .then(data => res.json({ ok: true, data: data }))
    .catch(error => res.json({ ok: false, error: error }));
});

router.post("/EditarPerfil", (req, res) => {
  upload(req, res, function(err) {
    if (err instanceof multer.MulterError) {
      return res.status(500).json(err);
    } else if (err) {
      return res.status(500).json(err);
    }
    const { token, Nombre, Apellido, Mail } = req.body;
    //si no existe el token no aceptamos logout
    if (!token) {
      return res.status(400).json({ ok: false, error: "token no recibido" });
    }
    console.log(req.file);
    Token.findOne({ where: { token } })
      .then(idUser => ModelUsuario.findOne({ where: { id: idUser.idUsuari } }))
      .then(item => {
        item.Nombre = Nombre;
        item.Apellido = Apellido;
        item.Img = req.file.filename;
        item.Mail = Mail;
        return item.save();
      })
      .then(data => res.status(200).send({ ok: true, data: data }));
  });
});

router.post("/AddInterest", (req, res) => {
  const { token, Categorias_id } = req.body;
  //si no existe el token no aceptamos logout
  if (!token) {
    return res.status(400).json({ ok: false, error: "token no recibido" });
  } else if (!Categorias_id) {
    return res.status(400).json({ ok: false, error: "Categoria no recibida" });
  }
  Token.findOne({ where: { token } })
    .then(idToken => idToken.idUsuari)
    .then(idUser =>
      model.Usuarios_Categorias.create({
        Usuarios_id: idUser,
        Categorias_id
      })
    )
    .then(data => res.json({ ok: true, data: data }))
    .catch(error => res.json({ ok: false, error: error }));
});

router.post("/RemoveInterest", (req, res) => {
  const { token, Categorias_id } = req.body;
  //si no existe el token no aceptamos logout
  if (!token) {
    return res.status(400).json({ ok: false, error: "token no recibido" });
  } else if (!Categorias_id) {
    return res.status(400).json({ ok: false, error: "Categoria no recibida" });
  }
  Token.findOne({ where: { token } })
    .then(idToken => idToken.idUsuari)
    .then(Usuarios_id =>
      model.Usuarios_Categorias.destroy({
        where: { Usuarios_id, Categorias_id }
      })
    )
    .then(data => res.json({ ok: true, data: data }))
    .catch(error => res.json({ ok: false, error: error }));
});

router.post("/VerUsuarios", (req, res) => {
  const { token } = req.body;
  //si no existe el token no aceptamos logout
  if (!token) {
    return res.status(400).json({ ok: false, error: "token no recibido" });
  }
  Token.findOne({ where: { token } })
    .then(() => model.Usuarios.findAll())
    .then(data => res.json({ ok: true, data: data }))
    .catch(error => res.json({ ok: false, error: error }));
});

router.delete("/logout", (req, res) => {
  const { token } = req.body;
  //si no existe el token no aceptamos logout
  if (!token) {
    return res.status(400).json({ ok: false, error: "token no recibido" });
  }
  // si lo recibimos, intentamos eliminarlo
  Token.destroy({ where: { token } })
    .then(() => res.json({ ok: true }))
    .catch(error => res.json({ ok: false, error: error }));
});

module.exports = router;
