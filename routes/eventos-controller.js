const express = require('express');
const multer = require('multer');
//bcrypt es un modulo que nos permite encriptar en una dirección

const modelo = require('../models/index');
const Token = modelo.Token;

const JWT = require('jsonwebtoken');
const SECRET = "merdolino";
const withAuth = require('../middleware');


//const tokenMaxMin = 30;

const router = express.Router();

// referencia:
//  https://programmingwithmosh.com/javascript/react-file-upload-proper-server-side-nodejs-easy/


//multer es un plugin que facilita la lectura de archivos procedentes de forms
//aquí se inicializa, indicando que la carpeta es 'uploads'
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname)
    }
})

const upload = multer({ storage: storage }).single('file');

router.all('/', (req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "http://localhost:1234");
    res.setHeader("Access-Control-Allow-Headers", "Authorization , Origin, X-Requested-With, Content-Type, Accept");
    res.setHeader("Access-Control-Allow-Methods", "POST, GET, DELETE, PUT, OPTIONS");
    res.setHeader("Access-Control-Allow-Credentials", "true");
    next();
})

router.all('/:xxx', (req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "http://localhost:1234");
    res.setHeader("Access-Control-Allow-Headers", "Authorization , Origin, X-Requested-With, Content-Type, Accept");
    res.setHeader("Access-Control-Allow-Methods", "POST, GET, DELETE, PUT, OPTIONS");
    res.setHeader("Access-Control-Allow-Credentials", "true");
    next();
})

router.get('/', (req, res, next) => {
    modelo.EventosView.findAll()
        .then(lista => res.json({ ok: true, data: lista }))
        .catch(err => res.json({ ok: false, error: err }));
});

router.get('/:id', (req, res, next) => {
    let idEventos = req.params.id;
    // modelo.Eventos.findById(idEventos)
    modelo.EventosView.findOne({ where: { id: idEventos } })
        .then(item => res.json({ ok: true, data: item }))
        .catch(err => res.json({ ok: false, error: err }));
});

router.post("/CrearEvento", (req, res) => {

    upload(req, res, function (err) {
        if (err instanceof multer.MulterError) {
            return res.status(500).json(err)
        } else if (err) {
            return res.status(500).json(err)
        }

        const { token, Nombre, Descripcion, Precio, FechaInicio, FechaFin, Latitud, Longitud, Telefono, Mail, Link } = req.body;
        //si no existe el token no aceptamos logout
        if (!token) {
            return res.status(400).json({ ok: false, error: "token no recibido" });
        }

        console.log(req.file);
        Token.findOne({ where: { token } })
            .then(tokenRow => modelo.Eventos.create({
                Nombre, Descripcion, Imagen: req.file.filename, Precio, FechaInicio, FechaFin, Latitud, Longitud, Telefono, Mail, Link, Usuarios_id: tokenRow.idUsuari
            }))
            .then((data) => res.status(200).send({ ok: true, data: data }));
    })
});

router.post("/EditarEvento", (req, res) => {

    upload(req, res, function (err) {
        if (err instanceof multer.MulterError) {
            return res.status(500).json(err)
        } else if (err) {
            return res.status(500).json(err)
        }
        const { id, token, Nombre, Descripcion, Precio, FechaInicio, FechaFin, Latitud, Longitud, Telefono, Mail, Link } = req.body;
        //si no existe el token no aceptamos logout
        if (!token) {
            return res.status(400).json({ ok: false, error: "token no recibido" });
        }
        console.log(req.file);
        Token.findOne({ where: { token } })
            .then(tokenRow => modelo.Eventos.findOne({ where: { id: id, Usuarios_id: tokenRow.idUsuari } }))
            .then(item => {
                item.Nombre = Nombre;
                item.Descripcion = Descripcion;
                item.Precio = Precio;
                item.FechaInicio = FechaInicio;
                item.FechaFin = FechaFin;
                item.Latitud = Latitud;
                item.Longitud = Longitud;
                item.Telefono = Telefono;
                item.Mail = Mail;
                item.Link = Link;
                item.Imagen = req.file.filename;
                item.Mail = Mail;
                return item.save();
            })
            .then((data) => res.status(200).send({ ok: true, err: data }));
    })
});

router.post("/AddCategory", (req, res) => {
    const { token, Eventos_id, Categorias_id } = req.body;
    //si no existe el token no aceptamos logout
    if (!token) {
        return res.status(400).json({ ok: false, error: "token no recibido" });
    }
    else if (!Categorias_id) {
        return res.status(400).json({ ok: false, error: "Categoria no recibida" });
    }
    else if (!Eventos_id) {
        return res.status(400).json({ ok: false, error: "Eventos no recibido" });
    }
    Token.findOne({ where: { token } })
        .then(idToken => idToken.idUsuari)
        .then(UserId => modelo.Eventos.findOne({ where: { id: Eventos_id, Usuarios_id: UserId } }))
        .then(() => modelo.Usuarios_Categorias.create({
            Eventos_id,
            Categorias_id
        }))
        .then(data => res.json({ ok: true, data: data }))
        .catch(error => res.json({ ok: false, error: error }));
});

router.post("/RemoveCategory", (req, res) => {
    const { token, Eventos_id, Categorias_id } = req.body;
    //si no existe el token no aceptamos logout
    if (!token) {
        return res.status(400).json({ ok: false, error: "token no recibido" });
    }
    else if (!Categorias_id) {
        return res.status(400).json({ ok: false, error: "Categoria no recibida" });
    }
    else if (!Eventos_id) {
        return res.status(400).json({ ok: false, error: "Eventos no recibido" });
    }
    Token.findOne({ where: { token } })
        .then(idToken => idToken.idUsuari)
        .then(UserId => modelo.Eventos.findOne({ where: { id: Eventos_id, Usuarios_id: UserId } }))
        .then(() => modelo.Usuarios_Categorias.destroy({
            where: { Eventos_id, Categorias_id }
        }))
        .then(data => res.json({ ok: true, data: data }))
        .catch(error => res.json({ ok: false, error: error }));
});

router.post("/RemoveEvento", (req, res) => {
    const { token, Eventos_id } = req.body;
    //si no existe el token no aceptamos logout
    if (!token) {
        return res.status(400).json({ ok: false, error: "token no recibido" });
    }
    else if (!Eventos_id) {
        return res.status(400).json({ ok: false, error: "Eventos no recibido" });
    }
    Token.findOne({ where: { token } })
        .then(() => modelo.Eventos.destroy({ where: { id: Eventos_id } }))
        .then(data => res.json({ ok: true, data: data }))
        .catch(error => res.json({ ok: false, error: error }));
});

router.post("/VerEventos", (req, res) => {
    const { token } = req.body;
    //si no existe el token no aceptamos logout
    if (!token) {
        return res.status(400).json({ ok: false, error: "token no recibido" });
    }
    Token.findOne({ where: { token } })
        .then(() => modelo.Eventos.findAll())
        .then(data => res.json({ ok: true, data: data }))
        .catch(error => res.json({ ok: false, error: error }));
});

router.post("/AddComment", (req, res) => {
    const { token, Eventos_id, Texto } = req.body;
    //si no existe el token no aceptamos logout
    if (!token) {
        return res.status(400).json({ ok: false, error: "token no recibido" });
    }
    else if (!Texto) {
        return res.status(400).json({ ok: false, error: "Categoria no recibida" });
    }
    else if (!Eventos_id) {
        return res.status(400).json({ ok: false, error: "Eventos no recibido" });
    }
    console.log(Texto);
    console.log(Eventos_id)
    console.log(token)
    Token.findOne({ where: { token } })
        .then((idToken) => modelo.Comentarios.create({
            Texto,
            Eventos_id,
            Usuarios_id: idToken.idUsuari
        }))
        .then(data => res.json({ ok: true, data: data }))
        .catch(error => res.json({ ok: false, error: error }));
});

module.exports = router;