const express = require('express');
const multer = require('multer');
//bcrypt es un modulo que nos permite encriptar en una dirección

const modelo = require('../models/index');
const Token = modelo.Token;

const JWT = require('jsonwebtoken');
const SECRET = "merdolino";
const withAuth = require('../middleware');

const router = express.Router();

router.get('/', (req, res, next) => {
    modelo.CategoriasView.findAll()
        .then(lista => res.json({ ok: true, data: lista }))
        .catch(err => res.json({ ok: false, error: err }));
});

router.post("/VerCategorias", (req, res) => {
    const { token } = req.body;
    //si no existe el token no aceptamos logout
    if (!token) {
        return res.status(400).json({ ok: false, error: "token no recibido" });
    }
    Token.findOne({ where: { token } })
        .then(() => modelo.Categorias.findAll())
        .then(data => res.json({ ok: true, data: data }))
        .catch(error => res.json({ ok: false, error: error }));
});

router.post("/NuevaCategorias", (req, res) => {
    const { token, Nombre, GruposCategoria_id } = req.body;
    //si no existe el token no aceptamos logout
    if (!token) {
        return res.status(400).json({ ok: false, error: "token no recibido" });
    }
    else if (!Nombre) {
        return res.status(400).json({ ok: false, error: "Nombre no recibida" });
    }
    else if (!GruposCategoria_id) {
        return res.status(400).json({ ok: false, error: "GruposCategoria_id no recibido" });
    }
    Token.findOne({ where: { token } })
        .then(() => modelo.Categorias.create({ Nombre, GruposCategoria_id }))
        .then(data => res.json({ ok: true, data: data }))
        .catch(error => res.json({ ok: false, error: error }));
});
router.post("/VerCategoriasView", (req, res) => {
    const { token } = req.body;
    //si no existe el token no aceptamos logout
    if (!token) {
        return res.status(400).json({ ok: false, error: "token no recibido" });
    }
    Token.findOne({ where: { token } })
        .then(() => modelo.CategoriasView.findAll())
        .then(data => res.json({ ok: true, data: data }))
        .catch(error => res.json({ ok: false, error: error }));
});

router.post("/VerGruposCategorias", (req, res) => {
    const { token } = req.body;
    //si no existe el token no aceptamos logout
    if (!token) {
        return res.status(400).json({ ok: false, error: "token no recibido" });
    }
    Token.findOne({ where: { token } })
        .then(() => modelo.GruposCategorias.findAll())
        .then(data => res.json({ ok: true, data: data }))
        .catch(error => res.json({ ok: false, error: error }));
});

router.post("/EliminarCategorias", (req, res) => {
    const { token, id } = req.body;
    //si no existe el token no aceptamos logout
    if (!token) {
        return res.status(400).json({ ok: false, error: "token no recibido" });
    }
    else if (!id) {
        return res.status(400).json({ ok: false, error: "GruposCategoria_id no recibido" });
    }
    Token.findOne({ where: { token } })
        .then(() => modelo.Categorias.destroy({ where: { id } }))
        .then(data => res.json({ ok: true, data: data }))
        .catch(error => res.json({ ok: false, error: error }));
});

module.exports = router;