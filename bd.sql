CREATE DATABASE  IF NOT EXISTS `apropmeu` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `apropmeu`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: apropmeu
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.38-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categorias`
--

DROP TABLE IF EXISTS `categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createdAt` date NOT NULL,
  `updatedAt` date NOT NULL,
  `Nombre` varchar(45) NOT NULL,
  `GruposCategorias_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Categorias_GruposCategorias_idx` (`GruposCategorias_id`),
  CONSTRAINT `fk_Categorias_GruposCategorias` FOREIGN KEY (`GruposCategorias_id`) REFERENCES `gruposcategorias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorias`
--

LOCK TABLES `categorias` WRITE;
/*!40000 ALTER TABLE `categorias` DISABLE KEYS */;
INSERT INTO `categorias` VALUES (1,'0000-00-00','0000-00-00','Clasica',1),(3,'0000-00-00','0000-00-00','Rap',1),(4,'0000-00-00','0000-00-00','Rugby',2),(5,'0000-00-00','0000-00-00','Handball',2);
/*!40000 ALTER TABLE `categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `categoriasx`
--

DROP TABLE IF EXISTS `categoriasx`;
/*!50001 DROP VIEW IF EXISTS `categoriasx`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `categoriasx` AS SELECT 
 1 AS `id`,
 1 AS `createdAt`,
 1 AS `updatedAt`,
 1 AS `Nombre`,
 1 AS `GruposCategorias_id`,
 1 AS `GrupoCategorias`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `comentarios`
--

DROP TABLE IF EXISTS `comentarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comentarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createdAt` date NOT NULL,
  `updatedAt` date NOT NULL,
  `Texto` longtext NOT NULL,
  `Eventos_id` int(11) NOT NULL,
  `Usuarios_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Comentarios_Eventos1_idx` (`Eventos_id`),
  KEY `fk_Comentarios_Usuarios1_idx` (`Usuarios_id`),
  CONSTRAINT `fk_Comentarios_Eventos1` FOREIGN KEY (`Eventos_id`) REFERENCES `eventos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Comentarios_Usuarios1` FOREIGN KEY (`Usuarios_id`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comentarios`
--

LOCK TABLES `comentarios` WRITE;
/*!40000 ALTER TABLE `comentarios` DISABLE KEYS */;
INSERT INTO `comentarios` VALUES (1,'0000-00-00','0000-00-00','No me gusta',2,1),(2,'0000-00-00','0000-00-00','Es muy entretenido',2,2);
/*!40000 ALTER TABLE `comentarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eventos`
--

DROP TABLE IF EXISTS `eventos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eventos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createdAt` date NOT NULL,
  `updatedAt` date NOT NULL,
  `Nombre` varchar(45) NOT NULL,
  `Descripcion` varchar(144) NOT NULL,
  `Imagen` varchar(45) NOT NULL DEFAULT 'defaultevent.png',
  `Precio` float NOT NULL,
  `FechaInicio` date NOT NULL,
  `FechaFin` date NOT NULL,
  `Latitud` float NOT NULL,
  `Longitud` float NOT NULL,
  `Telefono` int(11) DEFAULT NULL,
  `Mail` varchar(45) DEFAULT NULL,
  `Link` varchar(144) DEFAULT NULL,
  `Estado` varchar(45) NOT NULL DEFAULT 'Activo',
  `Usuarios_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Eventos_Usuarios1_idx` (`Usuarios_id`),
  CONSTRAINT `fk_Eventos_Usuarios1` FOREIGN KEY (`Usuarios_id`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eventos`
--

LOCK TABLES `eventos` WRITE;
/*!40000 ALTER TABLE `eventos` DISABLE KEYS */;
INSERT INTO `eventos` VALUES (2,'0000-00-00','0000-00-00','Evento Fortnite','Olakase','defaultevent.png',15,'0000-00-00','0000-00-00',50,50,NULL,NULL,NULL,'En Proceso',1),(3,'0000-00-00','0000-00-00','Event Test','Olap','defaultevent.png',16,'0000-00-00','0000-00-00',40,4,NULL,NULL,NULL,'Acabado',2),(4,'2019-09-26','2019-09-27','Viaje a Brasil','Viaje a Brasil con la casa con un autobus, lo paga FundacioEsplai','defaultevent.png',0,'2019-09-27','2019-09-29',0,0,0,'','','En Proceso',5);
/*!40000 ALTER TABLE `eventos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eventos_categorias`
--

DROP TABLE IF EXISTS `eventos_categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eventos_categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createdAt` date NOT NULL,
  `updatedAt` date NOT NULL,
  `Eventos_id` int(11) NOT NULL,
  `Categorias_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Eventos_Categorias_Eventos1_idx` (`Eventos_id`),
  KEY `fk_Eventos_Categorias_Categorias1_idx` (`Categorias_id`),
  CONSTRAINT `fk_Eventos_Categorias_Categorias1` FOREIGN KEY (`Categorias_id`) REFERENCES `categorias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Eventos_Categorias_Eventos1` FOREIGN KEY (`Eventos_id`) REFERENCES `eventos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eventos_categorias`
--

LOCK TABLES `eventos_categorias` WRITE;
/*!40000 ALTER TABLE `eventos_categorias` DISABLE KEYS */;
INSERT INTO `eventos_categorias` VALUES (9,'0000-00-00','0000-00-00',2,1),(10,'0000-00-00','0000-00-00',2,3);
/*!40000 ALTER TABLE `eventos_categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `eventosx`
--

DROP TABLE IF EXISTS `eventosx`;
/*!50001 DROP VIEW IF EXISTS `eventosx`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `eventosx` AS SELECT 
 1 AS `id`,
 1 AS `createdAt`,
 1 AS `updatedAt`,
 1 AS `Nombre`,
 1 AS `Descripcion`,
 1 AS `Imagen`,
 1 AS `Precio`,
 1 AS `FechaInicio`,
 1 AS `FechaFin`,
 1 AS `Latitud`,
 1 AS `Longitud`,
 1 AS `Telefono`,
 1 AS `Mail`,
 1 AS `Link`,
 1 AS `Estado`,
 1 AS `Usuarios_id`,
 1 AS `Asistentes`,
 1 AS `Autor`,
 1 AS `Categorias`,
 1 AS `Comentarios`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `gruposcategorias`
--

DROP TABLE IF EXISTS `gruposcategorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gruposcategorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createdAt` date NOT NULL,
  `updatedAt` date NOT NULL,
  `Nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gruposcategorias`
--

LOCK TABLES `gruposcategorias` WRITE;
/*!40000 ALTER TABLE `gruposcategorias` DISABLE KEYS */;
INSERT INTO `gruposcategorias` VALUES (1,'0000-00-00','0000-00-00','Musica'),(2,'0000-00-00','0000-00-00','Deportes');
/*!40000 ALTER TABLE `gruposcategorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tokens`
--

DROP TABLE IF EXISTS `tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(45) NOT NULL,
  `idUsuari` int(11) NOT NULL,
  `nomusuari` varchar(45) NOT NULL,
  `admin` tinyint(4) NOT NULL,
  `img` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tokens`
--

LOCK TABLES `tokens` WRITE;
/*!40000 ALTER TABLE `tokens` DISABLE KEYS */;
INSERT INTO `tokens` VALUES (1,'hzqlIIcmSsAAbS1',5,'Kira2',0,'ImagenDefault'),(2,'JDRnRsKZ9KBfst1',5,'Kira2',0,'ImagenDefault'),(3,'TWzSPdnj6N4e3Vm',5,'Kira2',0,'ImagenDefault'),(4,'7uJenHVH2FqPvM8',5,'Kira2',0,'ImagenDefault'),(5,'SBfpI06JRqB6STa',5,'Kira2',0,'ImagenDefault'),(6,'cooQYvxyMf8cAfe',5,'Kira2',0,'ImagenDefault'),(7,'J1fInkfETiUkzVL',5,'Kira2',0,'ImagenDefault'),(8,'99xiROBfaYnt9d8',5,'Kira2',0,'ImagenDefault'),(9,'1ZRfyfKHBOXTQqT',5,'Kira2',0,'ImagenDefault'),(10,'zs9nGcYyEFyTtpX',5,'Kira2',0,'ImagenDefault'),(11,'Jk06Fn253RovBBw',5,'Kira2',0,'ImagenDefault'),(12,'scPyT4vFWC0JGG0',5,'Kira2',0,'ImagenDefault'),(13,'nbRnwMsVdJXaBki',5,'Kira2',0,'ImagenDefault'),(14,'WWeVAVJ6NxPRClF',5,'Kira2',0,'ImagenDefault'),(15,'Xxj7RJLzt9CJTfz',5,'Kira2',0,'ImagenDefault'),(16,'n5auomY8A62iD67',5,'Kira2',0,'ImagenDefault'),(17,'kWCCLXA2DTwAMOn',5,'Kira2',0,'ImagenDefault'),(18,'J5ah14lyhR7z398',5,'Kira2',0,'ImagenDefault'),(19,'9bavynZwt1jBMPe',5,'Kira2',0,'ImagenDefault'),(20,'DOnBPr5rzIStuet',5,'Kira2',1,'ImagenDedefaultprofile.pngfault'),(21,'fiNkbvi8gDxDs5h',5,'Kira2',1,'defaultprofile.png'),(22,'AOIjAeMi30wxx05',5,'Kira2',1,'defaultprofile.png'),(23,'PppZY9aHxwMblPI',5,'Kira2',1,'defaultprofile.png'),(24,'y31BlfqXSQbcMm9',5,'Kira2',1,'defaultprofile.png'),(25,'Zow1cFWSkXLNFtM',5,'Kira2',1,'defaultprofile.png'),(26,'lHdLnjOLZ0Ie7rR',5,'Kira2',1,'defaultprofile.png'),(27,'daR8DH2M3WCXUVL',5,'Kira2',1,'defaultprofile.png'),(28,'rs2FalHbAPhlfxO',7,'Eric23',0,'defaultprofile.png'),(29,'h7Jj2v8wklpc2Be',5,'Kira2',1,'1569568914540-123834.jpg'),(30,'gHtDYDz72qPnyvm',5,'Kira2',1,'1569568914540-123834.jpg');
/*!40000 ALTER TABLE `tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createdAt` date NOT NULL,
  `updatedAt` date NOT NULL,
  `Nombre` varchar(45) DEFAULT 'Nombre',
  `Apellido` varchar(45) DEFAULT 'Apellido',
  `Usuario` varchar(45) NOT NULL,
  `Img` varchar(45) NOT NULL DEFAULT 'defaultprofile.png',
  `Mail` varchar(144) DEFAULT 'mail@mail.com',
  `Password` varchar(100) NOT NULL,
  `Admin` tinyint(4) DEFAULT '0',
  `Estado` varchar(45) DEFAULT 'Activo',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'0000-00-00','0000-00-00','Admin','Admin','Admin','defaultprofile.png','admin@apropmeu.com','Admin',0,'Activo'),(2,'0000-00-00','0000-00-00','Test','Test','Test','defaultprofile.png','test@apropmeu.com','Test',0,'Activo'),(3,'2019-09-23','2019-09-23','Nombre','Apellido','Kira0','defaultprofile.png','mail@mail.com','',0,NULL),(4,'2019-09-23','2019-09-23','Nombre','Apellido','Kira','defaultprofile.png','mail@mail.com','$2b$10$h9mlTyUgowrU2iETnJigreUs5mWwnBTg7fI/Qo',0,NULL),(5,'2019-09-23','2019-09-27','Nombre','Tepes','Kira2','defaultprofile.png','mail@mail.com','$2b$10$vRNDo8lpBZrkLPaJabZRneorRPfWzfIBm16m2tO4pNbRzmD3iWgW.',1,NULL),(6,'2019-09-27','2019-09-27','Nombre','Apellido','Eric','defaultprofile.png','ericmares13@gmail.com','$2b$10$oVyQwlLAqCTaYtDtkgmrEeR8bTBHNTEzz8ZDr008LFxhyXjqHkBIu',0,'Activo'),(7,'2019-09-27','2019-09-27','Nombre','Apellido','Eric23','defaultprofile.png','ericmares13@gmail.com','$2b$10$W5V9CaOQ.IX.hhcrO/XqmewALUIbGMnxd57.QjYnTpJAc9EPu.RyG',0,'Activo');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_categorias`
--

DROP TABLE IF EXISTS `usuarios_categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios_categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createdAt` date NOT NULL,
  `updatedAt` date NOT NULL,
  `Usuarios_id` int(11) NOT NULL,
  `Categorias_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Usuarios_Categorias_Usuarios1_idx` (`Usuarios_id`),
  KEY `fk_Categorias_id_idx` (`Categorias_id`),
  CONSTRAINT `fk_Categorias_id` FOREIGN KEY (`Categorias_id`) REFERENCES `categorias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Usuarios_Categorias_Usuarios1` FOREIGN KEY (`Usuarios_id`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_categorias`
--

LOCK TABLES `usuarios_categorias` WRITE;
/*!40000 ALTER TABLE `usuarios_categorias` DISABLE KEYS */;
INSERT INTO `usuarios_categorias` VALUES (1,'0000-00-00','0000-00-00',1,1),(2,'0000-00-00','0000-00-00',2,3),(4,'0000-00-00','0000-00-00',1,4);
/*!40000 ALTER TABLE `usuarios_categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_eventos`
--

DROP TABLE IF EXISTS `usuarios_eventos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios_eventos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createdAt` date NOT NULL,
  `updatedAt` date NOT NULL,
  `Usuarios_id` int(11) NOT NULL,
  `Eventos_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Usuarios_Eventos_Usuarios1_idx` (`Usuarios_id`),
  KEY `fk_Usuarios_Eventos_Eventos1_idx` (`Eventos_id`),
  CONSTRAINT `fk_Usuarios_Eventos_Eventos1` FOREIGN KEY (`Eventos_id`) REFERENCES `eventos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Usuarios_Eventos_Usuarios1` FOREIGN KEY (`Usuarios_id`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_eventos`
--

LOCK TABLES `usuarios_eventos` WRITE;
/*!40000 ALTER TABLE `usuarios_eventos` DISABLE KEYS */;
INSERT INTO `usuarios_eventos` VALUES (2,'0000-00-00','0000-00-00',1,2),(3,'0000-00-00','0000-00-00',2,2),(4,'0000-00-00','0000-00-00',1,3),(6,'2019-09-25','2019-09-25',5,2),(8,'2019-09-26','2019-09-26',5,4),(9,'2019-09-27','2019-09-27',5,3);
/*!40000 ALTER TABLE `usuarios_eventos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `usuariosx`
--

DROP TABLE IF EXISTS `usuariosx`;
/*!50001 DROP VIEW IF EXISTS `usuariosx`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `usuariosx` AS SELECT 
 1 AS `id`,
 1 AS `createdAt`,
 1 AS `updatedAt`,
 1 AS `Nombre`,
 1 AS `Apellido`,
 1 AS `Usuario`,
 1 AS `Img`,
 1 AS `Mail`,
 1 AS `Password`,
 1 AS `Admin`,
 1 AS `Estado`,
 1 AS `Categorias`,
 1 AS `Eventos`,
 1 AS `EventosCreados`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `categoriasx`
--

/*!50001 DROP VIEW IF EXISTS `categoriasx`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `categoriasx` AS select `categorias`.`id` AS `id`,`categorias`.`createdAt` AS `createdAt`,`categorias`.`updatedAt` AS `updatedAt`,`categorias`.`Nombre` AS `Nombre`,`categorias`.`GruposCategorias_id` AS `GruposCategorias_id`,`gruposcategorias`.`Nombre` AS `GrupoCategorias` from (`categorias` left join `gruposcategorias` on((`categorias`.`GruposCategorias_id` = `gruposcategorias`.`id`))) group by `categorias`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `eventosx`
--

/*!50001 DROP VIEW IF EXISTS `eventosx`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `eventosx` AS select `eventos`.`id` AS `id`,`eventos`.`createdAt` AS `createdAt`,`eventos`.`updatedAt` AS `updatedAt`,`eventos`.`Nombre` AS `Nombre`,`eventos`.`Descripcion` AS `Descripcion`,`eventos`.`Imagen` AS `Imagen`,`eventos`.`Precio` AS `Precio`,`eventos`.`FechaInicio` AS `FechaInicio`,`eventos`.`FechaFin` AS `FechaFin`,`eventos`.`Latitud` AS `Latitud`,`eventos`.`Longitud` AS `Longitud`,`eventos`.`Telefono` AS `Telefono`,`eventos`.`Mail` AS `Mail`,`eventos`.`Link` AS `Link`,`eventos`.`Estado` AS `Estado`,`eventos`.`Usuarios_id` AS `Usuarios_id`,count(distinct `usuarios_eventos`.`Usuarios_id`) AS `Asistentes`,`usuarios`.`Usuario` AS `Autor`,group_concat(distinct `categorias`.`Nombre` separator ',') AS `Categorias`,group_concat(distinct concat(`u2`.`Usuario`,',',`u2`.`Img`,',',`comentarios`.`Texto`) order by `comentarios`.`id` DESC separator ',') AS `Comentarios` from ((((((`eventos` left join `usuarios_eventos` on((`usuarios_eventos`.`Eventos_id` = `eventos`.`id`))) left join `usuarios` on((`usuarios`.`id` = `eventos`.`Usuarios_id`))) left join `eventos_categorias` on((`eventos_categorias`.`Eventos_id` = `eventos`.`id`))) left join `categorias` on((`eventos_categorias`.`Categorias_id` = `categorias`.`id`))) left join `comentarios` on((`comentarios`.`Eventos_id` = `eventos`.`id`))) left join `usuarios` `u2` on((`comentarios`.`Usuarios_id` = `u2`.`id`))) group by `usuarios_eventos`.`Eventos_id` order by `eventos`.`FechaInicio` desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `usuariosx`
--

/*!50001 DROP VIEW IF EXISTS `usuariosx`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `usuariosx` AS select `usuarios`.`id` AS `id`,`usuarios`.`createdAt` AS `createdAt`,`usuarios`.`updatedAt` AS `updatedAt`,`usuarios`.`Nombre` AS `Nombre`,`usuarios`.`Apellido` AS `Apellido`,`usuarios`.`Usuario` AS `Usuario`,`usuarios`.`Img` AS `Img`,`usuarios`.`Mail` AS `Mail`,`usuarios`.`Password` AS `Password`,`usuarios`.`Admin` AS `Admin`,`usuarios`.`Estado` AS `Estado`,group_concat(distinct concat(`categorias`.`id`,',',`categorias`.`Nombre`,',',`gruposcategorias`.`Nombre`) order by `categorias`.`id` DESC separator ',') AS `Categorias`,group_concat(distinct concat(`eventos`.`id`,',',`eventos`.`Nombre`,',',`eventos`.`Descripcion`,',',`eventos`.`Precio`) order by `eventos`.`id` DESC separator ',') AS `Eventos`,group_concat(distinct concat(`e2`.`id`,',',`e2`.`Nombre`,',',`e2`.`Descripcion`,',',`e2`.`Precio`) order by `eventos`.`id` DESC separator ',') AS `EventosCreados` from ((((((`usuarios` left join `usuarios_categorias` on((`usuarios_categorias`.`Usuarios_id` = `usuarios`.`id`))) left join `categorias` on((`categorias`.`id` = `usuarios_categorias`.`Categorias_id`))) left join `gruposcategorias` on((`gruposcategorias`.`id` = `categorias`.`GruposCategorias_id`))) left join `usuarios_eventos` on((`usuarios`.`id` = `usuarios_eventos`.`Usuarios_id`))) left join `eventos` on((`eventos`.`id` = `usuarios_eventos`.`Eventos_id`))) left join `eventos` `e2` on((`usuarios`.`id` = `e2`.`Usuarios_id`))) group by `usuarios`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-27 11:54:25
