CREATE VIEW EventosX AS SELECT eventos.*, 
COUNT(DISTINCT(usuarios_eventos.Usuarios_id)) AS Asistentes, 
Usuarios.Nombre AS Autor,
GROUP_CONCAT(DISTINCT(categorias.Nombre)) AS Categorias, 
GROUP_CONCAT(DISTINCT CONCAT(u2.Usuario,',',u2.Img,',',comentarios.Texto)ORDER BY comentarios.id DESC) AS Comentarios
FROM apropmeu.eventos 
LEFT JOIN apropmeu.usuarios_eventos ON usuarios_eventos.Eventos_id=Eventos.id  
LEFT JOIN apropmeu.Usuarios ON usuarios.id=Eventos.usuarios_id
LEFT JOIN apropmeu.eventos_categorias ON eventos_categorias.Eventos_id=eventos.id
LEFT JOIN apropmeu.categorias ON eventos_categorias.Categorias_id=categorias.id
LEFT JOIN apropmeu.comentarios ON comentarios.Eventos_id=eventos.id
LEFT JOIN apropmeu.Usuarios u2 ON comentarios.Usuarios_id=u2.id
GROUP BY usuarios_eventos.Eventos_id  ORDER BY FechaInicio DESC

CREATE VIEW Categoriasx AS SELECT categorias.*, GruposCategorias.Nombre AS GrupoCategorias
FROM apropmeu.categorias 
LEFT JOIN apropmeu.GruposCategorias ON categorias.GruposCategorias_id=GruposCategorias.id
GROUP BY apropmeu.categorias.id

CREATE VIEW Usuariosx AS SELECT usuarios.*,  
GROUP_CONCAT(DISTINCT CONCAT(Categorias.id,',',Categorias.Nombre,',',GruposCategorias.Nombre)ORDER BY categorias.id DESC) AS Categorias,
GROUP_CONCAT(DISTINCT CONCAT(Eventos.id,',',Eventos.Nombre,',',Eventos.Descripcion,',',Eventos.Precio)ORDER BY Eventos.id DESC) AS Eventos
FROM apropmeu.usuarios
LEFT JOIN apropmeu.Usuarios_Categorias ON Usuarios_Categorias.Usuarios_id=usuarios.id
LEFT JOIN apropmeu.Categorias ON Categorias.id=Usuarios_Categorias.Categorias_id
LEFT JOIN apropmeu.GruposCategorias ON GruposCategorias.id=GruposCategorias_id
LEFT JOIN apropmeu.Usuarios_Eventos ON Usuarios.id=Usuarios_Eventos.Usuarios_id
LEFT JOIN apropmeu.Eventos ON Eventos.id=Usuarios_Eventos.Eventos_id
GROUP BY usuarios.id