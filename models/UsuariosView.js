'use strict';

/*
CREATE VIEW Usuariosx AS SELECT usuarios.*,  
GROUP_CONCAT(DISTINCT CONCAT(Categorias.id,',',Categorias.Nombre,',',GruposCategorias.Nombre)ORDER BY categorias.id DESC) AS Categorias,
GROUP_CONCAT(DISTINCT CONCAT(Eventos.id,',',Eventos.Nombre,',',Eventos.Descripcion,',',Eventos.Precio)ORDER BY Eventos.id DESC) AS Eventos,
GROUP_CONCAT(DISTINCT CONCAT(e2.id,',',e2.Nombre,',',e2.Descripcion,',',e2.Precio)ORDER BY Eventos.id DESC) AS EventosCreados
FROM apropmeu.usuarios
LEFT JOIN apropmeu.Usuarios_Categorias ON Usuarios_Categorias.Usuarios_id=usuarios.id
LEFT JOIN apropmeu.Categorias ON Categorias.id=Usuarios_Categorias.Categorias_id
LEFT JOIN apropmeu.GruposCategorias ON GruposCategorias.id=GruposCategorias_id
LEFT JOIN apropmeu.Usuarios_Eventos ON Usuarios.id=Usuarios_Eventos.Usuarios_id
LEFT JOIN apropmeu.Eventos ON Eventos.id=Usuarios_Eventos.Eventos_id
LEFT JOIN apropmeu.Eventos e2 ON usuarios.id=e2.Usuarios_id
GROUP BY usuarios.id
*/

module.exports = (sequelize, DataTypes) => {
    const UsuariosView = sequelize.define('UsuariosView', {

        Nombre: DataTypes.STRING,
        Apellido: DataTypes.STRING,
        Usuario: {
            type: DataTypes.STRING,
            unique: true
        },
        Mail: DataTypes.STRING,
        Password: DataTypes.STRING,
        Img: DataTypes.STRING,
        Admin: DataTypes.BOOLEAN,
        Estado: DataTypes.STRING,
        Categorias: DataTypes.STRING,
        Eventos: DataTypes.STRING,
        EventosCreados: DataTypes.STRING

    }, { tableName: 'Usuariosx', timestamps: true });

    return UsuariosView;
};