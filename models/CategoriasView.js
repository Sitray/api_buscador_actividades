'use strict';

/*
CREATE VIEW Categoriasx AS SELECT categorias.*, GruposCategorias.Nombre AS GrupoCategorias
FROM apropmeu.categorias 
LEFT JOIN apropmeu.GruposCategorias ON categorias.GruposCategorias_id=GruposCategorias.id
GROUP BY apropmeu.categorias.id
*/

module.exports = (sequelize, DataTypes) => {
    const CategoriasView = sequelize.define('CategoriasView', {

        Nombre: DataTypes.STRING,
        GruposCategorias_id: DataTypes.INTEGER,
        GrupoCategorias: DataTypes.STRING

    }, { tableName: 'Categoriasx', timestamps: true });

    return CategoriasView;
};