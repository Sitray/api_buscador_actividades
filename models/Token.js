'use strict';

module.exports = (sequelize, DataTypes) => {
  const Token = sequelize.define('Token', {
    token: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    idUsuari: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    nomusuari: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    img: DataTypes.STRING,
    admin: {
      type: DataTypes.BOOLEAN
    }
    
  }, { tableName: 'Tokens', timestamps: false });
  
  return Token;
};
