'use strict';

module.exports = (sequelize, DataTypes) => {
    const Categorias = sequelize.define('Categorias', {

        Nombre: DataTypes.STRING,
        GruposCategorias_id: DataTypes.INTEGER

    }, { tableName: 'Categorias', timestamps: true });

    return Categorias;
};