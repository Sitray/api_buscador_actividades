'use strict';

/*
CREATE VIEW EventosX AS SELECT eventos.*, 
COUNT(DISTINCT(usuarios_eventos.Usuarios_id)) AS Asistentes, 
Usuarios.Usuario AS Autor,
GROUP_CONCAT(DISTINCT(categorias.Nombre)) AS Categorias, 
GROUP_CONCAT(DISTINCT CONCAT(u2.Usuario,',',u2.Img,',',comentarios.Texto)ORDER BY comentarios.id DESC) AS Comentarios
FROM apropmeu.eventos 
LEFT JOIN apropmeu.usuarios_eventos ON usuarios_eventos.Eventos_id=Eventos.id  
LEFT JOIN apropmeu.Usuarios ON usuarios.id=Eventos.usuarios_id
LEFT JOIN apropmeu.eventos_categorias ON eventos_categorias.Eventos_id=eventos.id
LEFT JOIN apropmeu.categorias ON eventos_categorias.Categorias_id=categorias.id
LEFT JOIN apropmeu.comentarios ON comentarios.Eventos_id=eventos.id
LEFT JOIN apropmeu.Usuarios u2 ON comentarios.Usuarios_id=u2.id
GROUP BY usuarios_eventos.Eventos_id  ORDER BY FechaInicio DESC
*/

module.exports = (sequelize, DataTypes) => {
    const EventosView = sequelize.define('EventosView', {

        Nombre: DataTypes.STRING,
        Descripcion: DataTypes.STRING,
        Imagen: DataTypes.STRING,
        Precio: DataTypes.FLOAT,
        FechaInicio: DataTypes.DATE,
        FechaFin: DataTypes.DATE,
        Latitud: DataTypes.FLOAT,
        Longitud: DataTypes.FLOAT,
        Telefono: DataTypes.INTEGER,
        Mail: DataTypes.STRING,
        Link: DataTypes.STRING,
        Estado: DataTypes.STRING,
        Usuarios_id: DataTypes.INTEGER,
        Asistentes: DataTypes.INTEGER,
        Autor: DataTypes.STRING,
        Categorias: DataTypes.STRING,
        Comentarios: DataTypes.STRING

    }, { tableName: 'Eventosx', timestamps: true });

    return EventosView;
};