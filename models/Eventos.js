'use strict';

module.exports = (sequelize, DataTypes) => {
    const Eventos = sequelize.define('Eventos', {

        Nombre: DataTypes.STRING,
        Descripcion: DataTypes.STRING,
        Imagen: DataTypes.STRING,
        Precio: DataTypes.FLOAT,
        FechaInicio: DataTypes.DATE,
        FechaFin: DataTypes.DATE,
        Latitud: DataTypes.FLOAT,
        Longitud: DataTypes.FLOAT,
        Telefono: DataTypes.INTEGER,
        Mail: DataTypes.STRING,
        Link: DataTypes.STRING,
        Estado: DataTypes.STRING,
        Usuarios_id: DataTypes.INTEGER

    }, { tableName: 'eventos', timestamps: true });

    return Eventos;
};