'use strict';

module.exports = (sequelize, DataTypes) => {
    const Usuarios_Categorias = sequelize.define('Usuarios_Categorias', {

        Usuarios_id: DataTypes.INTEGER,
        Categorias_id: DataTypes.INTEGER

    }, { tableName: 'Usuarios_Categorias', timestamps: true });

    return Usuarios_Categorias;
};