module.exports = (sequelize, DataTypes) => {
    const Usuarios = sequelize.define('Usuarios', {

        Nombre: DataTypes.STRING,
        Apellido: DataTypes.STRING,
        Usuario: {
            type: DataTypes.STRING,
            unique: true
        },
        Mail: DataTypes.STRING,
        Password: DataTypes.STRING,
        Img: DataTypes.STRING,
        Admin: DataTypes.BOOLEAN,
        Estado: DataTypes.STRING,

    }, { tableName: 'Usuarios' });

    return Usuarios;
};
