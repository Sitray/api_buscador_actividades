'use strict';

module.exports = (sequelize, DataTypes) => {
    const GruposCategorias = sequelize.define('GruposCategorias', {

        Nombre: DataTypes.INTEGER

    }, { tableName: 'GruposCategorias', timestamps: true });

    return GruposCategorias;
};