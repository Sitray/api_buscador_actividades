'use strict';

module.exports = (sequelize, DataTypes) => {
    const Usuarios_Eventos = sequelize.define('Usuarios_Eventos', {

        Usuarios_id: DataTypes.INTEGER,
        Eventos_id: DataTypes.INTEGER

    }, { tableName: 'Usuarios_Eventos', timestamps: true });

    return Usuarios_Eventos;
};