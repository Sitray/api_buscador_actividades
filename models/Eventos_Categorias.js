'use strict';

module.exports = (sequelize, DataTypes) => {
    const Eventos_Categorias = sequelize.define('Eventos_Categorias', {

        Eventos_id: DataTypes.INTEGER,
        Categorias_id: DataTypes.INTEGER

    }, { tableName: 'Eventos_Categorias', timestamps: true });

    return Eventos_Categorias;
};