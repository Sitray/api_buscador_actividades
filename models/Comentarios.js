'use strict';

module.exports = (sequelize, DataTypes) => {
    const Comentarios = sequelize.define('Comentarios', {

        Usuarios_id: DataTypes.INTEGER,
        Eventos_id: DataTypes.INTEGER,
        Texto: DataTypes.STRING

    }, { tableName: 'Comentarios', timestamps: true });

    return Comentarios;
};