const express = require("express");
const indexRouter = require("./routes/index-controller");
const eventosRouter = require("./routes/eventos-controller");
const usuariosRouter = require("./routes/usuarios-controller");
const categoriasRouter = require("./routes/categorias-controller");
const logger = require('morgan');
const cors = require("cors");

const app = express();
app.use(express.json());

app.use("/img", express.static('uploads'));
app.use(logger('dev')); //muestra en consola las peticiones Get, Post... recibidas
app.use(cors());


app.use("/", indexRouter);
app.use("/eventos", eventosRouter);
app.use("/usuarios", usuariosRouter);
app.use("/categorias", categoriasRouter);

const port = 3000;
app.listen(port, () => console.log("estamos en http://localhost:" + port));
